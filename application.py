from cs50 import SQL
from cs50 import get_string, get_int

# Configure CS50 Library to use SQLite database
db = SQL("sqlite:///project.db")

employee_list = []

while True:
# Define an employee, start shift, & end shift (program exit when no name input)
    employee = {"name" : "", "start_time": "", "end_time" : ""}

    name = get_string("Employee Name: ")
    if len(name)<1:
        break
    employee["name"] = name
    start = get_int("Start Time: ")
    employee["start_time"] = start
    end = get_int("End Time: ")
    employee["end_time"] = end
    employee_list.append(employee)

    # Create an empty list and append each hour worked to that list
    hourlist = []
    for i in range(start,end):
        hourlist.append(i)

    # Create an empty dictionary with each key being an the hour the business is open
    hours = {"12": "", "1": "", "2": "", "3":"", "4":"","5":"",
            "6":"", "7":"", "8":"", "9":"", "10":"", "11" : ""}

    # iterate over the keys in the dictionary checking to see if the the employee worked during that hour
    # If they did work, put a '1' in that spot, else 0.
    for key, item in hours.items():
        for hour in hourlist:
            if key == str(hour):
                hours[f'{key}'] = 1
                break
            else:
                hours[f'{key}'] = 0

    # insert my dictionary into a table row so that I know who worked, when worked, and how many on staff.
    db.execute(f"""insert into 'staff_schedule4'
                            ('name', 'twelvepm', 'onepm', 'twopm', 'threepm',
                            'fourpm', 'fivepm', 'sixpm', 'sevenpm', 'eightpm', 'ninepm', 'tenpm', 'elevenpm')
                    values ('{name}', '{hours['12']}', '{hours['1']}', '{hours['2']}', '{hours['3']}',
                             '{hours['4']}', '{hours['5']}', '{hours['6']}', '{hours['7']}', '{hours['8']}',
                             '{hours['9']}', '{hours['10']}', '{hours['11']}')""")
# Show the schedule
for employee in employee_list:
    print(f"{employee['name']} Start: {employee['start_time']}pm End: {employee['end_time']}pm")

#  Example query (how many staff are working at 6pm today?)
print(db.execute("SELECT sum(sixpm) from staff_schedule4 where date = CURRENT_DATE"))